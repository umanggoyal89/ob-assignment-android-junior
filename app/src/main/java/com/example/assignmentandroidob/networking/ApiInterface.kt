package com.example.mvvmkotlinexample.retrofit

import com.example.assignmentandroidob.model.response.ImagesDataModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET(ApiEndPoint.IMAGES)
    fun getImages(): Call<ImagesDataModel>

}