package com.example.mvvmkotlinexample.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.assignmentandroidob.model.response.ImagesDataModel
import com.example.mvvmkotlinexample.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MainActivityRepository {

    const val TAG: String = "REPOSITORY : "
    val responseModel = MutableLiveData<ImagesDataModel>()

    fun getImageApiCall(): MutableLiveData<ImagesDataModel> {

        val call = RetrofitClient.apiInterface.getImages()
        call.enqueue(object : Callback<ImagesDataModel> {
            override fun onFailure(call: Call<ImagesDataModel>, t: Throwable) {

                Log.v(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<ImagesDataModel>,
                response: Response<ImagesDataModel>
            ) {
                Log.v(TAG, response.body().toString())

                val data = response.body()

                responseModel.value = ImagesDataModel(data)
            }
        })

        return responseModel
    }
}