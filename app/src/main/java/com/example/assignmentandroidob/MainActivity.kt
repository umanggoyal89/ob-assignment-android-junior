package com.example.assignmentandroidob

import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.example.assignmentandroidob.databinding.ActivityMainBinding
import com.example.assignmentandroidob.model.response.ImagesDataModel
import com.example.assignmentandroidob.utils.LinearLayoutManagerWithSmoothScroller
import com.example.mvvmkotlinexample.viewmodel.MainActivityViewModel
import com.example.newsapp.adapters.ImagesDataAdapter

class MainActivity : AppCompatActivity() {
    private var linearLayoutManager: LinearLayoutManagerWithSmoothScroller? = null
    lateinit var mViewModel: MainActivityViewModel
    lateinit var mAdapter: ImagesDataAdapter
    private var isUserScroll = true
    companion object {
        lateinit var mBinding: ActivityMainBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setupViewModel()
        setObserver()
        initView()

    }

    private fun initView() {

        linearLayoutManager = LinearLayoutManagerWithSmoothScroller(this)
        mBinding.rvImages.layoutManager = linearLayoutManager
        mBinding.rvImages.addOnItemTouchListener(onItemTouchListener);
    }

    private fun setObserver() {
        mViewModel.getImages()!!.observe(this, Observer { imagesData ->

            if (imagesData.status!! == "success")
                setDataViews(imagesData.data)


        })

    }

    private fun setDataViews(data: List<ImagesDataModel.Datum>?) {
        mAdapter = ImagesDataAdapter(this@MainActivity)
        mBinding.rvImages.adapter = mAdapter
        mAdapter.addAll(data)


        mBinding.rvImages.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(rv: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(rv, dx, dy)
                val visibleItemCount: Int = linearLayoutManager!!.getChildCount()
                val totalItemCount: Int = linearLayoutManager!!.getItemCount()
                val firstVisibleItemPosition: Int =
                    linearLayoutManager!!.findFirstVisibleItemPosition()
                val lastItem = firstVisibleItemPosition + visibleItemCount
                Log.d(
                    "positions", "onScrolled: " + visibleItemCount
                            + " >> " + totalItemCount + " >> "
                            + firstVisibleItemPosition + " >> "
                            + lastItem
                )

                val actionBar: ActionBar? = supportActionBar
                if (actionBar != null) {
                    //Setting a dynamic title at runtime. Here, it displays the current item.
                    ("Item "+(firstVisibleItemPosition+1)).also { actionBar.title = it }
                }


            }
        })

    }

    private fun setupViewModel() {
        mViewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

    }

    var onItemTouchListener: OnItemTouchListener = object : OnItemTouchListener {
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {

            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
    }
}