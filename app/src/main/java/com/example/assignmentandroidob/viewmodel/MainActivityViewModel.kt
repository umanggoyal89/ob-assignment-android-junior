package com.example.mvvmkotlinexample.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.assignmentandroidob.model.response.ImagesDataModel
import com.example.mvvmkotlinexample.repository.MainActivityRepository

class MainActivityViewModel : ViewModel() {

    var resLiveData: MutableLiveData<ImagesDataModel>? = null

    fun getImages(): LiveData<ImagesDataModel>? {
        resLiveData = MainActivityRepository.getImageApiCall()
        return resLiveData
    }

}