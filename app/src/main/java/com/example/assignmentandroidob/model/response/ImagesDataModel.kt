package com.example.assignmentandroidob.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ImagesDataModel(data: ImagesDataModel?) {
    val message: String? = data!!.message
    val status: String? = data!!.status
    val data: List<Datum>? = data!!.data

    class Datum {
        @SerializedName("item1")
        @Expose
        val item1: String? = null

        @SerializedName("item2")
        @Expose
        val item2: String? = null

        @SerializedName("item3")
        @Expose
        val item3: String? = null

        @SerializedName("item4")
        @Expose
        val item4: String? = null

        @SerializedName("item5")
        @Expose
        val item5: String? = null

    }

}

