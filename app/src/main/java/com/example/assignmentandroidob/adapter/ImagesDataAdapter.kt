package com.example.newsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentandroidob.R
import com.example.assignmentandroidob.model.response.ImagesDataModel

import com.squareup.picasso.Picasso



class ImagesDataAdapter(context: Context) : RecyclerView.Adapter<ImagesDataAdapter.ViewHolder>() {
    private var mContext: Context? = context
    private var arrListNewsDetails: List<ImagesDataModel.Datum?>? =
        java.util.ArrayList<ImagesDataModel.Datum>()

    fun addAll(arrList: List<ImagesDataModel.Datum?>?) {
        this.arrListNewsDetails = arrList
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvItemHead: TextView = itemView.findViewById(R.id.tv_itemHead)
        val imgOne: ImageView = itemView.findViewById(R.id.img_one)
        val imgTwo: ImageView = itemView.findViewById(R.id.img_two)
        val imgThree: ImageView = itemView.findViewById(R.id.img_three)
        val imgFour: ImageView = itemView.findViewById(R.id.img_four)
        val imgFive: ImageView = itemView.findViewById(R.id.img_five)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.info_row_image, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position > 0) {
            holder.tvItemHead.visibility = View.VISIBLE
            ("Item " + (position + 1)).also { holder.tvItemHead.text = it }
        } else {
            holder.tvItemHead.visibility = View.GONE
        }


        holder.imgOne.scaleType = ImageView.ScaleType.FIT_XY;
        holder.imgTwo.scaleType = ImageView.ScaleType.FIT_XY;
        holder.imgThree.scaleType = ImageView.ScaleType.CENTER_CROP;
        holder.imgFour.scaleType = ImageView.ScaleType.FIT_XY;
        holder.imgFive.scaleType = ImageView.ScaleType.FIT_XY;

        Picasso.get().load(arrListNewsDetails?.get(position)?.item1)

            .into(holder.imgOne)
        Picasso.get().load(arrListNewsDetails?.get(position)?.item2)

            .into(holder.imgTwo)
        Picasso.get().load(arrListNewsDetails?.get(position)?.item3)
            .into(holder.imgThree)
        Picasso.get().load(arrListNewsDetails?.get(position)?.item4)

            .into(holder.imgFour)
        Picasso.get().load(arrListNewsDetails?.get(position)?.item5)

            .into(holder.imgFive)

    }

    override fun getItemCount(): Int {
        return arrListNewsDetails!!.size
    }
}